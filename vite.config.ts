// @ts-check
import { defineConfig } from "vite";
import path from "path";

import vue from "@vitejs/plugin-vue2";

import childProcess from "child_process";

const __VERSION__ = JSON.stringify(
  `${childProcess
    .execSync("git describe --tags --always --first-parent")
    .toString()}`
);

export default defineConfig({
  define: {
    "import.meta.env.VUE_APP_GIT_HASH": __VERSION__,
  },
  plugins: [vue()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
});
