import { Group, User } from '../entities';

export function getSortedGroupsById(ids: number[], groups: Map<number, Group>): Group[] {
  return ids
    .map((x) => groups.get(x))
    .filter((x): x is Group => !!x)
    .sort((o1, o2) => o1.name.localeCompare(o2.name));
}

export function getImplicitlyOwnedGroups(
  user: User,
  _groups: Map<number, Group> | Group[]
): Group[] {
  const groupMap = Array.isArray(_groups)
    ? _groups.reduce((acc, cur) => {
        acc.set(cur.id, cur);
        return acc;
      }, new Map<number, Group>())
    : _groups;

  return user.groups
    .map((groupId) => groupMap.get(groupId))
    .filter((it): it is Group => !!it)
    .map((it) => it.ownedGroups)
    .flat()
    .concat(user.ownedGroups)
    .map((groupId) => groupMap.get(groupId))
    .filter((it): it is Group => !!it);
}
