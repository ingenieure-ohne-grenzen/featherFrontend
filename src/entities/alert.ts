export type Alert = {
  type: 'error' | 'warning' | 'success';
  uniqueType:
    | 'network'
    | 'unauthorized'
    | 'forbidden'
    | 'server_error'
    | 'unexpected'
    | 'unknown'
    | { id: string };
  short: string;
  description: string | string[];
};
