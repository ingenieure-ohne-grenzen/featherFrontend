import { useApplicationStore } from '../../stores/application';
import { axios, runCatching } from '../util';
import moment from 'moment';

export type UserResponse = {
  id: number;
  username: String;
  displayName: String;
  firstname: String;
  surname: String;
  mail: String;
  groups: Set<number>;
  registeredSince: string;
  ownedGroups: Set<number>;
  permissions: Set<string>;
  disabled: boolean;
  lastLoginAt: string;
};

export async function getAllUsers(): Promise<UserResponse[]> {
  return await runCatching<UserResponse[]>(() => axios.get('/users')).then((it) => it.data);
}

export async function getInactiveUsers(): Promise<[UserResponse[], moment]> {
  const inactive_period =
    useApplicationStore().config?.bindings.multiservice.userSettings.inactiveUsersSettings
      ?.reminderAfterInactivityInMonths;
  if (inactive_period == null) return [[], null];
  const inactivityThreshold = moment().subtract(inactive_period, 'months');
  const users = await getAllUsers();
  return [
    users.filter((u) =>
      moment.max(moment(u.lastLoginAt), moment(u.registeredSince)).isBefore(inactivityThreshold)
    ),
    inactivityThreshold,
  ];
}
