import PrintGdpr from '../views/print/PrintGdpr.vue';
import PrintLayout from '../components/PrintLayout.vue';

export default {
  path: '/print',
  component: PrintLayout,
  children: [
    {
      path: 'gdpr/:gdprId',
      name: 'Datenschutzerklärung ausdrucken',
      component: PrintGdpr,
      meta: {
        miniSessionAllowed: true,
      },
    },
  ],
};
