import { defineStore } from 'pinia';
import * as entities from '../entities';

const initialConfig: () => {
  config: entities.config.Config | undefined;
  session: entities.Session | undefined;
  alerts: entities.Alert[];
  logoutRequest: boolean;
} = () => ({
  config: undefined,
  session: undefined,
  alerts: [],
  logoutRequest: false,
});

export const useApplicationStore = defineStore('application', {
  state: initialConfig,
  actions: {
    setConfig(config: entities.config.Config) {
      this.config = config;
    },
    setSession(session: entities.Session) {
      this.session = session;
    },
    clearSession() {
      this.logoutRequest = false;
      this.session = undefined;
    },
    clearAlerts() {
      this.alerts = [];
    },
    pushAlert(alert: entities.Alert) {
      // Only push alert, if the same unique type is not here already
      // custom alerts will all be displayed
      const uniqueType = alert.uniqueType;

      if (typeof uniqueType === 'string') {
        if (this.alerts.some((a) => a.uniqueType == uniqueType)) {
          return;
        }
      } else {
        // Replace alert if already exists
        this.alerts = this.alerts.filter(
          (it) =>
            typeof it.uniqueType === 'string' ||
            (typeof it.uniqueType === 'object' && it.uniqueType.id != uniqueType.id)
        );
      }

      this.alerts.push(alert);
    },
    logoutRequested() {
      this.logoutRequest = true;
    },
  },
  getters: {
    getAlerts: (state) => {
      return state.alerts;
    },
  },
});
